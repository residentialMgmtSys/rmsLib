/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.api.user;

/**
 *
 * @author ayojava
 */
public interface UserIdentityDTOPath {
    
    String V1_API="/userIdentityDTO/v1";
    
    String LOGIN_API="/login/{userName}/{password}";
    
    String LIST_USER_IDENTITY_DTO_API="/list";
}
