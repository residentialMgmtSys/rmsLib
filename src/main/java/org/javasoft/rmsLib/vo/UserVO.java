/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.vo;

import java.time.LocalDateTime;
import lombok.Data;

/**
 *
 * @author ayojava
 */
@Data
public class UserVO {
    
    private Long userId;
    
    private String userName;
    
    private String fullName;
       
    private String profileCode;
    
    private String profileName;
        
    private LocalDateTime loginDateTime;
    
    private String locale;
}
