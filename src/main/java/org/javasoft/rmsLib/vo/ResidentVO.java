/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.vo;

import lombok.Data;

/**
 *
 * @author ayojava
 */
@Data
public class ResidentVO {
    
    private int realResident;
    
    private int virtualResident;
    
    public void updateRealResident(){
        setRealResident(getRealResident() + 1);
    }
    
    public void updateVirtualResident(){
        setVirtualResident(getVirtualResident() + 1);
    }
}
