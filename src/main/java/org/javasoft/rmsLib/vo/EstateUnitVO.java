/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.vo;

import lombok.Data;

/**
 *
 * @author ayojava
 */
@Data
public class EstateUnitVO {
    
    private int availableResidentialUnits;
    
    private int occupiedResidentialUnits;
    
    private int damagedResidentialUnits;
    
    private int availableNonResidentialUnits;
    
    private int occupiedNonResidentialUnits;
    
    private int damagedNonResidentialUnits;
    
    private int totalResidentialUnits;
    
    private int totalNonResidentialUnits;
    
    public void updateAvailableResidentialUnits(){
        setAvailableResidentialUnits(getAvailableResidentialUnits() + 1);
    }
    
    public void updateOccupiedResidentialUnits(){
         setOccupiedResidentialUnits(getAvailableResidentialUnits() + 1);
    }
    
    public void updateDamagedResidentialUnits(){
        setDamagedResidentialUnits(getDamagedResidentialUnits() + 1);
    }
    
    public void updateAvailableNonResidentialUnits(){
        setAvailableNonResidentialUnits(getAvailableNonResidentialUnits() + 1);
    }
    
    public void updateOccupiedNonResidentialUnits(){
         setOccupiedNonResidentialUnits(getAvailableNonResidentialUnits() + 1);
    }
    
    public void updateDamagedNonResidentialUnits(){
        setDamagedNonResidentialUnits(getDamagedNonResidentialUnits() + 1);
    }
    
    public int getTotalResidentialUnits(){
        return availableResidentialUnits + occupiedResidentialUnits + damagedResidentialUnits;
    }
    
    public int getTotalNonResidentialUnits(){
        return availableNonResidentialUnits + occupiedNonResidentialUnits + damagedNonResidentialUnits;
    }
}
