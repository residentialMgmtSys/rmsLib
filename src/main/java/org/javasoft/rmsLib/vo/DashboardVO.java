/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.vo;

import java.util.Date;
import lombok.Data;

/**
 *
 * @author ayojava
 */
@Data
public class DashboardVO {
         
    private Date serverStartTime;
    
    private long loggedInResidentProfile;
    
    private long loggedInAdminProfile ;
}
