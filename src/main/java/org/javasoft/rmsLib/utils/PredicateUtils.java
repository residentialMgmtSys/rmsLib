/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils;

import java.util.function.BiPredicate;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author ayojava
 */
public interface PredicateUtils {

    BiPredicate<String, String> COMPARE_STRING_VALUES = (val1, val2) -> StringUtils.equalsIgnoreCase(val1, val2);

    BiPredicate<Integer, Integer> COMPARE_INT_VALUES = (val1, val2) -> val1.compareTo(val2) == 0;

    BiPredicate<Long, Long> COMPARE_LONG_VALUES = (val1, val2) -> val1.compareTo(val2) == 0;
    
    

}
