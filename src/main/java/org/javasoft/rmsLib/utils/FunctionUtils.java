/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.javasoft.rmsLib.dto.facility.EstateUnitDTO;
import org.javasoft.rmsLib.dto.occupants.ResidentDTO;
import org.javasoft.rmsLib.vo.EstateUnitVO;
import org.javasoft.rmsLib.vo.ResidentVO;

/**
 *
 * @author ayojava
 */
public interface FunctionUtils {

    Function<String[], String> APPEND_STRINGS = (String[] arrayStr) -> {
        return Arrays.stream(arrayStr).collect(Collectors.joining());
    };

    Function<List<ResidentDTO>, ResidentVO> CONVERT_TO_RESIDENTVO = (List<ResidentDTO> residentDTOs) -> {
        ResidentVO residentVO = new ResidentVO();
        residentDTOs.stream().forEach(aResidentDTO -> {
            if (aResidentDTO.isRealResident()) {
                residentVO.updateRealResident();
            }
            if (aResidentDTO.isVirtualResident()) {
                residentVO.updateVirtualResident();
            }
        });
        return residentVO;
    };

    Function<List<EstateUnitDTO>, EstateUnitVO> CONVERT_TO_ESTATEVO = (List<EstateUnitDTO> estateUnitDTOs) -> {
        EstateUnitVO estateUnitVO = new EstateUnitVO();
        estateUnitDTOs.stream().forEach(aEstateUnitDTO -> {
            if (aEstateUnitDTO.isResidentialUnit()) {
                if (aEstateUnitDTO.isAvailable()) {
                    estateUnitVO.updateAvailableResidentialUnits();
                }
                if (aEstateUnitDTO.isDamaged()) {
                    estateUnitVO.updateDamagedResidentialUnits();
                }
                if (aEstateUnitDTO.isOccupied()) {
                    estateUnitVO.updateOccupiedResidentialUnits();
                }
            }
            if (aEstateUnitDTO.isNonResidentialUnit()) {
                if (aEstateUnitDTO.isAvailable()) {
                    estateUnitVO.updateAvailableNonResidentialUnits();
                }
                if (aEstateUnitDTO.isDamaged()) {
                    estateUnitVO.updateDamagedNonResidentialUnits();
                }
                if (aEstateUnitDTO.isOccupied()) {
                    estateUnitVO.updateOccupiedNonResidentialUnits();
                }
            }
        });
        return estateUnitVO;
    };

}
