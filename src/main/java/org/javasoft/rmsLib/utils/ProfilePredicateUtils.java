/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils;

import java.util.function.Predicate;
import static org.javasoft.rmsLib.utils.constants.RmsConstants.ADMIN_PROFILE;
import static org.javasoft.rmsLib.utils.constants.RmsConstants.RESIDENT_PROFILE;
import static org.javasoft.rmsLib.utils.constants.RmsConstants.VENDOR_PROFILE;

/**
 *
 * @author ayojava
 */
public interface ProfilePredicateUtils extends PredicateUtils{
    
    Predicate<String> ADMIN_PROFILE_PREDICATE = (val)-> COMPARE_STRING_VALUES.test(val, ADMIN_PROFILE);
    
    Predicate<String> RESIDENT_PROFILE_PREDICATE = (val)-> COMPARE_STRING_VALUES.test(val, RESIDENT_PROFILE);
    
    Predicate<String> VENDOR_PROFILE_PREDICATE = (val)-> COMPARE_STRING_VALUES.test(val, VENDOR_PROFILE);
}
