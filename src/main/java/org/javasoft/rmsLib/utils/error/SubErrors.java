/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.error;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author ayojava
 */
@Data
@AllArgsConstructor
public class SubErrors {

    private String className;
    
    private String field;
    
    private Object rejectedValue;
    
    private String message;

    public SubErrors(String className, String field) {
        this.className = className;
        this.field = field;
    }
    
    
}
