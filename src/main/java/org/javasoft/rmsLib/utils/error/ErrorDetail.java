/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.error;

import java.util.Date;
import java.util.List;
import lombok.Data;

/**
 *
 * @author ayojava
 */
@Data
public class ErrorDetail {
        
    /**
     * provides a brief title of the Error Condition
     */
    private String title;
    
    /**
     * Contains the http status code for the current request
     */
    private int httpStatusCode;
        
    /**
     * contains a short description of the error
     */
    private String detail;
    
    /**
     * Time when error occurred
     */
    private Date date;
    
    /**
     * info such as exception class name or stack trace
     */
    private String message;
    
    
    private List<SubErrors> subErrorsList;
}
