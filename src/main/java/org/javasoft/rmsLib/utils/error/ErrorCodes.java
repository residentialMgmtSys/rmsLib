/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.error;

/**
 *
 * @author ayojava
 */
public interface ErrorCodes {
    
    String E001 = "An Application Error has occured";
        
    String E002 = "Resource Not Found";
    
    String E003 = "A Database Error Has Occured";
    
    String E004 = "A ConstraintViolation Exception has occurred";
    
    String E005 = "An Invalid Method Arguement Exception has occurred";
    
    String E006 = "Unsupported Media Type";
    
    String E007 = "";
    
    String E008 = "";
    
    String E030 = "An Exception has occurred during Login, Contact the Administrator";
    
    String E031 = "Incorrect Login Details";
}
