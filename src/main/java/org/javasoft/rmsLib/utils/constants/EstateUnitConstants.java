/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.constants;

/**
 *
 * @author ayojava
 */
public interface EstateUnitConstants {
    
    public static  final String CONTAINER = "CN";
    
    public static  final String SHOP = "SH";
    
    public static  final String GATEHOUSE = "GH";
    
    public static  final String BARBING = "BB";
    
    public static  final String BUSINESS_CENTRE="BC";
    
    public static  final String GROCERIES_STORE="GS"; 
    
    public static  final String OFFICE="OF"; 
    
    public static final String RESIDENTIAL_UNIT ="RU";
    
    public static final String NON_RESIDENTIAL_UNIT ="NRU";
    
    public static final String RESIDENTIAL_UNITS_GROUP ="RUG";
    
    public static final String NON_RESIDENTIAL_UNITS_GROUP ="NRUG";
    
    public static final String ESTATE_UNITS_GROUP ="EUG";
    
    public static final String ZONE_A ="A";
    
    public static final String ZONE_B ="B";
    
    public static final String ZONE_C ="C";
    
    public static final String ZONE_D ="D";
    
}
