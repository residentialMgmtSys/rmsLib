/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.constants;

/**
 *
 * @author ayojava
 */
public interface RmsConstants {
    
    String DEFAULT_REST_PATH="http://localhost:7777/rmsRest";
        
    String DEFAULT_MAIL_TYPE="gmail";
    
    String GMAIL="gmail";
    
    int UPLOAD_FILE_SIZE=1024000;
    
    String SEPARATOR = "|";
    
    String SLASH = "/";
    
    String SAVE_ACTION = "S";
    
    String UPDATE_ACTION = "U";
    
    String ADD_ACTION = "A";
    
    String LOWER_CASE = "LC";
    
    String UPPER_CASE = "UC";
    
    String DIGIT = "DG";
    
    String SPECIAL_CHARACTER = "SC";
        
    String APPROVAL_FN="AP";
    
    String DELETE_FN="DL";
    
    String EDIT_FN="ED";
    
    String LIST_FN="LS";
    
    String NEW_FN="NW";
    
    String VIEW_FN="VW";
    
    String TEMP="tmp";
    
    String ADMIN_PROFILE = "AD";

    String RESIDENT_PROFILE = "RS";

    String VENDOR_PROFILE = "VN";
}
