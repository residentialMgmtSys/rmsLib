/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.constants;

/**
 *
 * @author ayojava
 */
public interface ResidentConstants {
    
    String REAL_RESIDENT ="RR";
    
    String VIRTUAL_RESIDENT ="VR";
    
    String CHRISTIAN ="C";
    
    String MUSLIM ="M";
    
    String OTHER ="O";
    
    String MALE ="M";
    
    String FEMALE ="F";
    
    String SINGLE ="S";
    
    String MARRIED ="M";
    
    String MR ="Mr";
    
    String MRS ="Mrs";
    
    String MS ="Ms";
    
    String MISS ="Miss";
    
    String DR ="Dr";
    
    String PROF ="Prof";
        
    String REAL_RESIDENTS_GROUP ="RRG";
    
    String VIRTUAL_RESIDENTS_GROUP ="VRG";
    
    String RESIDENTS_GROUP ="RG";
}
