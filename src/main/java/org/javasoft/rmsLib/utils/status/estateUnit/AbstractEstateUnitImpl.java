/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.status.estateUnit;

import org.javasoft.rmsLib.dto.DefaultDTO;

/**
 *
 * @author ayojava
 */
public abstract class AbstractEstateUnitImpl extends DefaultDTO implements EstateUnitStatusIntf{
    
    public boolean isMovedOut(){
        return COMPARE_STRING_VALUES.test(getStatus(),MOVED_OUT);
    }
    
    public void setMovedOut(){
        setStatus(MOVED_OUT);
    }
    
    public void setAvailable(){
        setStatus(AVAILABLE);
    }
    
    public boolean isAvailable(){
        return COMPARE_STRING_VALUES.test(getStatus(),AVAILABLE);
    }
    
    public void setOccupied(){
        setStatus(OCCUPIED);
    }  
    
    public boolean isOccupied(){
        return COMPARE_STRING_VALUES.test(getStatus(),OCCUPIED);
    }
    
    public boolean isDamaged(){
        return COMPARE_STRING_VALUES.test(getStatus(),DAMAGED);
    }
    
    
}
