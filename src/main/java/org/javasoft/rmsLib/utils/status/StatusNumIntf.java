/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.status;

/**
 *
 * @author ayojava
 */
public interface StatusNumIntf {
    
    void setStatus(int status);

    public int getStatus();
    
}
