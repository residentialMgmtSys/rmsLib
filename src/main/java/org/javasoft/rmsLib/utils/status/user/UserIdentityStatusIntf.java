/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.status.user;

import org.javasoft.rmsLib.utils.status.StatusIntf;
import org.javasoft.rmsLib.utils.status.StatusNames;

/**
 *
 * @author ayojava
 */
public interface UserIdentityStatusIntf extends StatusIntf{
    
    public static final String LOGGED_OUT = "LO";

    public static final String LOGGED_IN = "LI";
    
    default String getSystemUserStatusName(String statusName){
        switch(statusName){
            case LOGGED_OUT:
                return StatusNames.LOGGED_OUT;
            case LOGGED_IN:
                return StatusNames.LOGGED_IN;
        }
        return "";
    }
}
