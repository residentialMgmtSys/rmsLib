/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.status.estateUnit;

import java.util.function.Function;
import org.javasoft.rmsLib.utils.status.StatusIntf;
import org.javasoft.rmsLib.utils.status.StatusNames;

/**
 *
 * @author ayojava
 */
public interface EstateUnitStatusIntf extends StatusIntf {

    public static final String AVAILABLE = "AV";

    public static final String OCCUPIED = "OC";

    public static final String DAMAGED = "DM";

    public static final String MOVED_OUT = "MO";

    default Function<String, String> estateStatusNameFn() {
        return (String statusName) -> {
            switch (statusName) {
                case AVAILABLE:
                    return StatusNames.AVAILABLE;
                case DAMAGED:
                    return StatusNames.DAMAGED;
                case OCCUPIED:
                    return StatusNames.OCCUPIED;
                case MOVED_OUT:
                    return StatusNames.MOVED_OUT;
            }
            return "";
        };
    }

}
