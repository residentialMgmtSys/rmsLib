/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.status.entityState;

import java.util.function.Function;
import org.javasoft.rmsLib.utils.status.StatusNames;
import org.javasoft.rmsLib.utils.status.StatusNumIntf;

/**
 *
 * @author ayojava
 */
public interface EntityStateIntf extends StatusNumIntf {

    public static final int INITIALIZED = -1;

    public static final int NEW = 0;

    public static final int APPROVED = 1;

    public static final int MODIFIED = 2;

    public static final int NEWLY_MODIFIED = 3;

    public static final int REJECTED = 4;

    public static final int DELETED = 5;

    default Function<Integer, String> getEntityStateName() {
        return (Integer entityState) -> {
            switch (entityState) {
                case INITIALIZED:
                    return StatusNames.ENTITY_INITIALIZED;
                case NEW:
                    return StatusNames.ENTITY_NEW;
                case APPROVED:
                    return StatusNames.ENTITY_APPROVED;
                case MODIFIED:
                    return StatusNames.ENTITY_MODIFIED;
                case NEWLY_MODIFIED:
                    return StatusNames.ENTITY_NEWLY_MODIFIED;
                case REJECTED:
                    return StatusNames.ENTITY_REJECTED;
            }
            return "";
        };
    }
}
