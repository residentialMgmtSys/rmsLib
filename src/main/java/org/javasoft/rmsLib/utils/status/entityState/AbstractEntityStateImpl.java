/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.status.entityState;

import org.javasoft.rmsLib.dto.DefaultDTO;
import static org.javasoft.rmsLib.utils.status.entityState.EntityStateIntf.APPROVED;
import static org.javasoft.rmsLib.utils.status.entityState.EntityStateIntf.MODIFIED;
import static org.javasoft.rmsLib.utils.status.entityState.EntityStateIntf.NEW;

/**
 *
 * @author ayojava
 */
public abstract class AbstractEntityStateImpl extends DefaultDTO implements EntityStateIntf{
    
    public void setNew() {
        setStatus(NEW);
    }

    public void setApproved() {
        setStatus(APPROVED);
    }

    public void setRejected() {
        setStatus(REJECTED);
    }

    public void setModified() {
        setStatus(MODIFIED);
    }

    public void setNewlyModified() {
        setStatus(MODIFIED);
    }
    
    public void setInitialized(){
        setStatus(INITIALIZED);
    }
    
    
       
}
