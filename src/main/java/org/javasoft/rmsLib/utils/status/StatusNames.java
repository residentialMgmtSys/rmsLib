/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.status;

/**
 *
 * @author ayojava
 */
public interface StatusNames {
    
    public static final String LOGGED_OUT ="Logged Out";
    
    public static final String LOGGED_IN ="Logged In";
    
    public static final String AVAILABLE ="Available";
    
    public static final String OCCUPIED="Occupied";
    
    public static final String DAMAGED="Damaged";
    
    public static final String MOVED_OUT="Moved Out";
    
    public static final String ENTITY_INITIALIZED="Initialized";
    
    public static final String ENTITY_NEW="New";
    
    public static final String ENTITY_APPROVED="Approved";
    
    public static final String ENTITY_MODIFIED="Modified";
    
    public static final String ENTITY_NEWLY_MODIFIED="Newly Modified";
    
    public static final String ENTITY_REJECTED="Rejected";
}
