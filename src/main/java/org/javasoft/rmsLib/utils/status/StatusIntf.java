/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.status;

import org.javasoft.rmsLib.utils.PredicateUtils;

/**
 *
 * @author ayojava
 */
public interface StatusIntf extends PredicateUtils{
    
    void setStatus(String status);

    public String getStatus(); 
    
}
