/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.status.user;

import org.javasoft.rmsLib.utils.flag.AbstractGenericFlagImpl;

/**
 *
 * @author ayojava
 */
public abstract class AbstractUserIdentityImpl extends AbstractGenericFlagImpl implements UserIdentityStatusIntf {

    public boolean isLoggedOut(){
        return COMPARE_STRING_VALUES.test(getStatus(),LOGGED_OUT);
    }
    
    public void setLoggedOut(){
        setStatus(LOGGED_OUT);
    }
        
    public boolean isLoggedIn(){
        return COMPARE_STRING_VALUES.test(getStatus(),LOGGED_IN);
    }
    
    public void setLoggedIn(){
        setStatus(LOGGED_IN);
    }
    
}
