/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils;

import java.util.function.IntPredicate;
import static org.javasoft.rmsLib.utils.status.entityState.EntityStateIntf.APPROVED;
import static org.javasoft.rmsLib.utils.status.entityState.EntityStateIntf.MODIFIED;
import static org.javasoft.rmsLib.utils.status.entityState.EntityStateIntf.NEW;
import static org.javasoft.rmsLib.utils.status.entityState.EntityStateIntf.NEWLY_MODIFIED;

/**
 *
 * @author ayojava
 */
public interface EntityStatePredicateUtils extends PredicateUtils{
    
    IntPredicate ENTITY_STATE_APPROVED_PREDICATE = es -> COMPARE_INT_VALUES.test(es,APPROVED);
   
    
    IntPredicate ENTITY_STATE_MODIFIED_PREDICATE = es -> COMPARE_INT_VALUES.test(es,MODIFIED);
     
    
    IntPredicate ENTITY_STATE_NEW_PREDICATE = es -> COMPARE_INT_VALUES.test(es,NEW);
        
    
    IntPredicate ENTITY_STATE_NEWLY_MODIFIED_PREDICATE = es -> COMPARE_INT_VALUES.test(es,NEWLY_MODIFIED);
        
    
    IntPredicate ENTITY_STATE_APPROVED = ENTITY_STATE_APPROVED_PREDICATE.or(ENTITY_STATE_MODIFIED_PREDICATE);
    
    
    IntPredicate ENTITY_STATE_PENDING = ENTITY_STATE_NEWLY_MODIFIED_PREDICATE.or(ENTITY_STATE_NEW_PREDICATE);
}
