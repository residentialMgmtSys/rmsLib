/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import lombok.extern.slf4j.Slf4j;
import sun.misc.BASE64Encoder;

/**
 *
 * @author ayojava
 */
@Slf4j
public class PasswordUtil {

    public String encrypt(String plainText) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA");
            md.update(plainText.getBytes("UTF-8"));
            byte raw[] = md.digest();
            return (new BASE64Encoder()).encode(raw);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            log.error("An Exception has occured" ,ex);
        }
        return plainText;
    }
    //tSHKpuHbguWgHJJKQZhwy3K4FjU=
    public static void main(String [] args){
        System.out.println("::::: " + new PasswordUtil().encrypt("ADMIN"));
    }
}
