/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.flag;

import java.util.function.Function;

/**
 *
 * @author ayojava
 */
public interface GenericFlagIntf extends FlagIntf {

    public static final String ACTIVE = "AC";

    public static final String INACTIVE = "IN";

    public static final String LOCKED = "LK";

    public static final String SUSPENDED = "SP";

    default Function<String, String> getFlagName() {
        return (String flagName) -> {
            switch (flagName) {
                case ACTIVE:
                    return FlagNames.ACTIVE;
                case INACTIVE:
                    return FlagNames.INACTIVE;
                case LOCKED:
                    return FlagNames.LOCKED;
                case SUSPENDED:
                    return FlagNames.SUSPENDED;
            }
            return "";
        };
    }
}
