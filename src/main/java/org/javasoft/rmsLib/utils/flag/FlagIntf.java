/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.flag;

import org.apache.commons.lang3.StringUtils;
import org.javasoft.rmsLib.utils.PredicateUtils;

/**
 *
 * @author ayojava
 */
public interface FlagIntf extends PredicateUtils{
       
    void setFlag(String flag);

    public String getFlag();
    
    default boolean compareFlagValues(String val1, String val2){
        return StringUtils.equalsIgnoreCase(val1,val2);
    }
}
