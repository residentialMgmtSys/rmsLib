/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.flag;

/**
 *
 * @author ayojava
 */
public interface FlagNames {
    
    public static final String ACTIVE = "Active";
    
    public static final String INACTIVE = "Inactive";
    
    public static final String LOCKED="Locked";
    
    public static final String SUSPENDED="Suspended";
}
