/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.utils.flag;

import org.javasoft.rmsLib.dto.DefaultDTO;

/**
 *
 * @author ayojava
 */
public abstract class AbstractGenericFlagImpl extends DefaultDTO implements GenericFlagIntf{
    
    public boolean isActive(){
        return COMPARE_STRING_VALUES.test(getFlag(), ACTIVE) ;
    }
    
    public void setActive(){
        setFlag(ACTIVE);
    }
    
    public boolean isSuspended(){
        return COMPARE_STRING_VALUES.test(getFlag(), SUSPENDED) ;
    }
    
    public void setSuspended(){
        setFlag(SUSPENDED);
    }
    
    public boolean isLocked(){
        return COMPARE_STRING_VALUES.test(getFlag(), LOCKED);
    }
    
    public void setLocked(){
        setFlag(LOCKED);
    }
    
    public boolean isInActive(){
        return COMPARE_STRING_VALUES.test(getFlag(), INACTIVE);
    }
    
    public void setInActive(){
        setFlag(INACTIVE);
    }
    
}
