/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javasoft.rmsLib.dto.facility.EstateUnitDTO;
import org.javasoft.rmsLib.dto.flow.EntityStateDTO;
import org.javasoft.rmsLib.dto.group.UserIdentityGroupDTO;
import org.javasoft.rmsLib.dto.occupants.ResidentDTO;
import org.javasoft.rmsLib.utils.status.user.AbstractUserIdentityImpl;


/**
 *
 * @author ayojava
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserIdentityDTO extends AbstractUserIdentityImpl{
    
    private Long userIdentityId;
    
    public UserIdentityDTO(boolean dataAvailable){
        super.setDataAvailable(dataAvailable);
    }
    
    private String userName; 
    
    private String passwordHash;
    
    private String systemNo;
    
    private String flag;
    
    private String status;
        
    private boolean sysAdmin; 
    
    private EstateUnitDTO estateUnit;
    
    private ResidentDTO residentDetails;
    
    private EntityStateDTO entityState;
    
    private List<UserIdentityGroupDTO> userIdentityGroups;
    
    private List<UserProfileDTO> UserProfileDTOs;
    
    public String getDTOFlagName(){
        return getFlagName().apply(getFlag());
    }
}
