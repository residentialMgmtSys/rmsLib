/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javasoft.rmsLib.dto.DefaultDTO;
import org.javasoft.rmsLib.dto.settings.core.MenuDTO;
import org.javasoft.rmsLib.dto.settings.core.ModuleDTO;
import org.javasoft.rmsLib.dto.settings.core.ProfileDTO;

/**
 *
 * @author ayojava
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserProfileDTO extends DefaultDTO{
    
    private Long userProfileId;
    
    public UserProfileDTO(boolean dataAvailable){
        super.setDataAvailable(dataAvailable);
    }
    
    private ProfileDTO profile;
    
    private ModuleDTO module;
    
    private MenuDTO menu;
}
