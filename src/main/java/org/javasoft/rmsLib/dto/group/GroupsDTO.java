/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.group;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javasoft.rmsLib.dto.settings.core.MenuDTO;
import org.javasoft.rmsLib.dto.settings.core.ModuleDTO;
import org.javasoft.rmsLib.utils.flag.AbstractGenericFlagImpl;

/**
 *
 * @author ayojava
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupsDTO extends AbstractGenericFlagImpl{
    
    private Long groupsId;

    private String groupName;
    
    private String groupCode;
    
    private String flag;
    
    private boolean defaultGrp;
    
    private String subMenu;
    
    public GroupsDTO(Long groupsId, String groupName, String groupCode, String flag, boolean defaultGrp, String subMenu, Date createDate) {
        this.groupsId = groupsId;
        this.groupName = groupName;
        this.groupCode = groupCode;
        this.flag = flag;
        this.defaultGrp = defaultGrp;
        this.subMenu = subMenu;
        super.setCreateDate(createDate);
    }
        
    private ModuleDTO module;
    
    private MenuDTO menu;
}
