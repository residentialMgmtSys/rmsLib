/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.group;

import java.util.List;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.javasoft.rmsLib.dto.user.UserIdentityDTO;

/**
 *
 * @author ayojava
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserIdentityGroupDTO extends GroupsDTO{
    
    private List<UserIdentityDTO> allUserIdentity;
}
