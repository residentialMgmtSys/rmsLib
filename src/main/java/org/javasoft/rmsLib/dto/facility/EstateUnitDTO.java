/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.facility;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import static java.util.stream.Collectors.joining;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javasoft.rmsLib.dto.account.EstateUnitAccountDTO;
import org.javasoft.rmsLib.dto.flow.EntityStateDTO;
import org.javasoft.rmsLib.dto.group.EstateUnitGroupDTO;
import org.javasoft.rmsLib.dto.user.UserIdentityDTO;
import static org.javasoft.rmsLib.utils.constants.EstateUnitConstants.NON_RESIDENTIAL_UNIT;
import static org.javasoft.rmsLib.utils.constants.EstateUnitConstants.RESIDENTIAL_UNIT;
import org.javasoft.rmsLib.utils.PredicateUtils;
import org.javasoft.rmsLib.utils.status.estateUnit.AbstractEstateUnitImpl;

/**
 *
 * @author ayojava
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class EstateUnitDTO extends AbstractEstateUnitImpl implements PredicateUtils {

    public EstateUnitDTO(boolean dataAvailable) {
        setDataAvailable(dataAvailable);
    }

    protected Long estateUnitId;

    protected Long actualEstateUnitId;

    protected String unitType;

    protected String status;//AVAILABLE. OCCUPIED . DAMAGED . MOVED OUT

    protected int unitSequence;

    protected String estateUnitCode;

    protected String groupNames;

    protected EstateUnitAccountDTO estateUnitAccount;

    protected EntityStateDTO entityState;

    private List<UserIdentityDTO> userIdentitys;

    private List<EstateUnitGroupDTO> estateUnitGroups;

    public String getGroupNames() {
        if (estateUnitGroups != null && !estateUnitGroups.isEmpty()) {
            return estateUnitGroups.stream().map(EstateUnitGroupDTO::getGroupName).collect(joining(","));
        } else {
            return "No Group Name";
        }
    }

    public boolean isResidentialUnit() {
        return COMPARE_STRING_VALUES.test(getUnitType(), RESIDENTIAL_UNIT);
    }

    public boolean isNonResidentialUnit() {
        return COMPARE_STRING_VALUES.test(getUnitType(), NON_RESIDENTIAL_UNIT);
    }
}
