/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.facility;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.javasoft.rmsLib.dto.facility.template.NonResidentialAddressTemplateDTO;
import static org.javasoft.rmsLib.utils.constants.EstateUnitConstants.BARBING;
import static org.javasoft.rmsLib.utils.constants.EstateUnitConstants.BUSINESS_CENTRE;
import static org.javasoft.rmsLib.utils.constants.EstateUnitConstants.CONTAINER;
import static org.javasoft.rmsLib.utils.constants.EstateUnitConstants.GATEHOUSE;
import static org.javasoft.rmsLib.utils.constants.EstateUnitConstants.GROCERIES_STORE;
import static org.javasoft.rmsLib.utils.constants.EstateUnitConstants.OFFICE;
import static org.javasoft.rmsLib.utils.constants.EstateUnitConstants.SHOP;

/**
 *
 * @author ayojava
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class NonResidentialUnitDTO extends EstateUnitDTO{
    
    private String nonResidentialType;//CONTAINER, SHOP
    
    private String activity;//
    
    private NonResidentialAddressTemplateDTO nonResidentialAddressTemplate;
    
    private String typeName;

    private String activityName;
    
    @Override
    public String getEstateUnitCode(){
        StringBuilder builder = new StringBuilder();
        String zoneCode=getNonResidentialAddressTemplate().getZoneCode();
        builder = builder.append(zoneCode).append(getNonResidentialAddressTemplate().getIdentificationNo()).append(getUnitType());
        return builder.toString();
    }
    
    public String getTypeName(){
        switch(getNonResidentialType()){
            case CONTAINER:
                return "Container";
            case SHOP:
                return "Shop";
            case GATEHOUSE:
                return "Gatehouse";
        }
        return "";
    }
    
    public String getActivityName(){
        switch (getActivity()){
            case BARBING:
                return "Barbing";
            case BUSINESS_CENTRE:
                return "Business Centre";
            case GROCERIES_STORE:
                return "Groceries Store";
            case OFFICE :
                return "Office";
        }
        return "";
    }
}
