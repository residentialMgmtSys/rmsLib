/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.facility;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.javasoft.rmsLib.dto.facility.template.ResidentialAddressTemplateDTO;
import static org.javasoft.rmsLib.utils.constants.RmsConstants.SLASH;

/**
 *
 * @author ayojava
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ResidentialUnitDTO extends EstateUnitDTO{
    
    private int residentCount;
    
    private ResidentialAddressTemplateDTO addressTemplate;
    
    @Override
    public String getEstateUnitCode(){
        StringBuilder builder = new StringBuilder();
        builder = builder.append(addressTemplate.getZoneCode());
        builder = builder.append(SLASH);
        builder = builder.append(StringUtils.leftPad(String.valueOf(addressTemplate.getBlockNum()), 3, "0"));
        builder = builder.append(SLASH);
        builder = builder.append(StringUtils.leftPad(String.valueOf(addressTemplate.getFlatNum()), 2, "0"));
        builder = builder.append(SLASH);
        builder = builder.append(getUnitType());
        return builder.toString();
    }
}
