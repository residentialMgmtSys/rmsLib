/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.flow;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javasoft.rmsLib.utils.EntityStatePredicateUtils;
import org.javasoft.rmsLib.utils.status.entityState.AbstractEntityStateImpl;

/**
 *
 * @author ayojava
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class EntityStateDTO extends AbstractEntityStateImpl implements EntityStatePredicateUtils{
    
    public EntityStateDTO(boolean dataAvailable){
        super.setDataAvailable(dataAvailable);
    }
    
    private Long entityStateId;
    
    private int status;
    
    public boolean isApproved(){
        return ENTITY_STATE_APPROVED.test(status);
    }
    
    public boolean isPending(){
        return ENTITY_STATE_PENDING.test(status);
    }
}
