/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.util;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javasoft.rmsLib.dto.DefaultDTO;
import org.javasoft.rmsLib.dto.facility.EstateUnitDTO;
import org.javasoft.rmsLib.dto.occupants.ResidentDTO;
import org.javasoft.rmsLib.dto.settings.core.ModuleDTO;
import org.javasoft.rmsLib.dto.settings.core.ProfileDTO;

/**
 *
 * @author ayojava
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InitDTO extends DefaultDTO{
    
    private List<EstateUnitDTO> estateUnitDTOs;
    
    private List<ResidentDTO> residentDTOs;
    
    private List<ProfileDTO> profileDTOs;
    
    private List<ModuleDTO> moduleDTOs;
}
