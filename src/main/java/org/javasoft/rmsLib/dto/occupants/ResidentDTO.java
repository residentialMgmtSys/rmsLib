/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.occupants;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javasoft.rmsLib.dto.DefaultDTO;
import org.javasoft.rmsLib.dto.flow.EntityStateDTO;
import static org.javasoft.rmsLib.utils.constants.ResidentConstants.REAL_RESIDENT;
import static org.javasoft.rmsLib.utils.constants.ResidentConstants.VIRTUAL_RESIDENT;
import org.javasoft.rmsLib.utils.PredicateUtils;

/**
 *
 * @author ayojava
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResidentDTO extends DefaultDTO implements PredicateUtils{
    
    public ResidentDTO(boolean dataAvailable){
        super.setDataAvailable(dataAvailable);
    }
    
    private Long residentId;
    
    private String title;

    private String firstName;
    
    private String lastName;

    private String identificationNo;
    
    private String emailAddress;
    
    private String altEmailAddress;
    
    private String phoneNum;
    
    private String altPhoneNum;
    
    private String occupation;

    private String sex;
    
    private String religion;

    private String maritalStatus;
    
    private String residentType;
    
    private Date entryDate;//moved in the estate

    private Date createDate;

    private Date birthday;
    
    private String fullName;

    public ResidentDTO(Long residentId, String title, String firstName, String lastName, String identificationNo, String emailAddress, 
            String altEmailAddress, String phoneNum, String altPhoneNum, String occupation, String sex, String religion, 
            String maritalStatus, String residentType, Date entryDate, Date createDate, Date birthday,String fullName) {
        this.residentId = residentId;
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.identificationNo = identificationNo;
        this.emailAddress = emailAddress;
        this.altEmailAddress = altEmailAddress;
        this.phoneNum = phoneNum;
        this.altPhoneNum = altPhoneNum;
        this.occupation = occupation;
        this.sex = sex;
        this.religion = religion;
        this.maritalStatus = maritalStatus;
        this.residentType = residentType;
        this.entryDate = entryDate;
        this.createDate = createDate;
        this.birthday = birthday;
        this.fullName = fullName;
    }
    
    private EntityStateDTO entityState;
    
    public boolean isRealResident() {
        return COMPARE_STRING_VALUES.test(getResidentType(),REAL_RESIDENT);
    }

    public boolean isVirtualResident() {
        return COMPARE_STRING_VALUES.test(getResidentType(),VIRTUAL_RESIDENT);
    }
    
    public boolean isResidentApproved(){
        return entityState.isApproved();
    }
    
    public boolean isResidentPending(){
        return entityState.isPending();
    }
    
    
}
