/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.settings.core;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javasoft.rmsLib.dto.DefaultDTO;
import org.javasoft.rmsLib.utils.ProfilePredicateUtils;

/**
 *
 * @author ayojava
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfileDTO extends DefaultDTO implements ProfilePredicateUtils{
    
    public ProfileDTO(boolean dataAvailable){
        super.setDataAvailable(dataAvailable);
    }
    
    private Long profileId;
    
    private String profileName;
    
    private String profileCode;
    
    private String flag;
                
    public boolean isAdminProfile(){
        return ADMIN_PROFILE_PREDICATE.test(profileCode);
    }
    
    public boolean isResidentProfile(){
        return RESIDENT_PROFILE_PREDICATE.test(profileCode);
    }
    
    public boolean isVendorProfile(){
        return VENDOR_PROFILE_PREDICATE.test(profileCode);
    }
}
