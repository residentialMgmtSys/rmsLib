/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.settings.core;

import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javasoft.rmsLib.dto.DefaultDTO;
import org.javasoft.rmsLib.utils.PredicateUtils;

/**
 *
 * @author ayojava
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ModuleDTO extends DefaultDTO implements PredicateUtils{
    
    public ModuleDTO(boolean dataAvailable){
        super.setDataAvailable(dataAvailable);
    }
    
    private Long moduleId;

    private String moduleCode;

    private String moduleName;

    private String flag;
    
    public ModuleDTO(Long moduleId, String moduleCode, String moduleName, String flag, Date createDate) {
        this.moduleId = moduleId;
        this.moduleCode = moduleCode;
        this.moduleName = moduleName;
        this.flag = flag;
        super.setCreateDate(createDate);
    }
    
    private ProfileDTO profileDTO;
    
    private List<MenuDTO> menuDTOs;
}
