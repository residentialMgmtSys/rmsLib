/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.settings.core;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.regex.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javasoft.rmsLib.dto.DefaultDTO;
import static org.javasoft.rmsLib.utils.constants.RmsConstants.APPROVAL_FN;
import static org.javasoft.rmsLib.utils.constants.RmsConstants.DELETE_FN;
import static org.javasoft.rmsLib.utils.constants.RmsConstants.EDIT_FN;
import static org.javasoft.rmsLib.utils.constants.RmsConstants.LIST_FN;
import static org.javasoft.rmsLib.utils.constants.RmsConstants.NEW_FN;
import static org.javasoft.rmsLib.utils.constants.RmsConstants.SEPARATOR;
import static org.javasoft.rmsLib.utils.constants.RmsConstants.VIEW_FN;

/**
 *
 * @author ayojava
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class FunctionsDTO extends DefaultDTO{
    
    private boolean approvalFn;
    
    private boolean deleteFn;
    
    private boolean editFn;
    
    private boolean listFn;
    
    private boolean newFn;
    
    private boolean viewFn;
    
    public String[] functionAsArray(){
        StringBuilder stringBuilderObj = new StringBuilder();
        if (isApprovalFn()) {
            stringBuilderObj = stringBuilderObj.append(APPROVAL_FN).append(SEPARATOR);
        }
        if (isDeleteFn()) {
            stringBuilderObj = stringBuilderObj.append(DELETE_FN).append(SEPARATOR);
        }
        if (isEditFn()) {
            stringBuilderObj = stringBuilderObj.append(EDIT_FN).append(SEPARATOR);
        }
        if (isListFn()) {
            stringBuilderObj = stringBuilderObj.append(LIST_FN).append(SEPARATOR);
        }
        if (isNewFn()) {
            stringBuilderObj = stringBuilderObj.append(NEW_FN).append(SEPARATOR);
        }
        if (isViewFn()) {
            stringBuilderObj = stringBuilderObj.append(VIEW_FN).append(SEPARATOR);
        }
        
        return stringBuilderObj.toString().split(Pattern.quote(SEPARATOR));
    }
    
    public void functionAsBoolean(String... arrays){
        for (String array : arrays) {
            switch (array) {
                case APPROVAL_FN:
                    setApprovalFn(true);
                    break;
                case DELETE_FN:
                    setDeleteFn(true);
                    break;
                case EDIT_FN:
                    setEditFn(true);
                    break;
                case LIST_FN:
                    setListFn(true);
                    break;
                case NEW_FN:
                    setNewFn(true);
                    break;
                case VIEW_FN:
                    setViewFn(true);
                    break;
            }
        }
    }
}
