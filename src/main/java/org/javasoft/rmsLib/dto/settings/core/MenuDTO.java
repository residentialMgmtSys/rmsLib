/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.settings.core;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javasoft.rmsLib.dto.DefaultDTO;
import org.javasoft.rmsLib.utils.PredicateUtils;

/**
 *
 * @author ayojava
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuDTO extends DefaultDTO implements PredicateUtils{
    
    public MenuDTO(boolean dataAvailable){
        super.setDataAvailable(dataAvailable);
    }
    
    private Long menuId;

    private String menuCode;
    
    private String menuName;
    
    private String flag; 
        
    private FunctionsDTO functionsDTO;
    
}
