/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto;

import java.util.Date;
import lombok.Data;

/**
 *
 * @author ayojava
 */
@Data
public class DefaultDTO {
    
    private boolean dataAvailable = true; // assumes that their is  data set in the object
    
    private Date createDate;
    
}
