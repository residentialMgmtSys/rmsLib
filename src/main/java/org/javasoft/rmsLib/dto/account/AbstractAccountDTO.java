/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.account;

import lombok.Data;
import org.javasoft.rmsLib.dto.DefaultDTO;

/**
 *
 * @author ayojava
 */
@Data
public class AbstractAccountDTO extends DefaultDTO{
    
    private Long account_id;
    
    private String accountNo;

    private String accountName;

    private String systemAccountNo; // system generated which is unique and will be unchanged 
        
    private String flag ; // Active or Locked or Suspended 
    
    private Double accountBalance ;
        
    private AccountTemplateDTO accountTemplate;
}
