/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsLib.dto.account;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javasoft.rmsLib.dto.DefaultDTO;
import org.javasoft.rmsLib.dto.flow.EntityStateDTO;

/**
 *
 * @author ayojava
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountTemplateDTO extends DefaultDTO{
    
    private Long account_template_id;
    
    private String accountType ; 
    
    private String txnFlow ;
    
    private boolean overdraftAllowed;
        
    private EntityStateDTO entityState;
}
